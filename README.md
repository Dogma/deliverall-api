# Deliverall
Sistema para gerenciamento de encomendas.<br />
Consiste em uma API desenvolvida para gerenciar encomendas diversas (principalmente de restaurantes).

## Tecnologias usadas
Tecnologia Java framework Spring Boot;

## Implementações de interfacesse de usuario:
-A primeira trata-se de um site desenvolvido com a tecnologia javascript framework React;<br />
-A segunda trata-se de um sistema mobile desenvolvido com React Native.

## Servidor em desenvolvimento
Todas as informações necessárias para a manipulação da api podem ser encontradas no endereço https://documenter.getpostman.com/view/4624983/RWEfMK4c

## Acessar a API
O acesso pode ser feito pelo link https://deliverall-api.herokuapp.com/<br />
Link para a documentação da API https://documenter.getpostman.com/view/4624983/S1Lu18xZ

## Link para Acessar a Interface de usuário
O Projeto da interface de usuario (site) pode ser encontrado no endereço https://gitlab.com/Dogma/sushibar-ui<br />
O projeto da interface mobile pode ser encontrado no endereço https://gitlab.com/Dogma/shushibar-mobile

## Outras informações sobre o projeto podem ser encontrardos na Wiki.
link para a Wiki https://gitlab.com/Dogma/deliverall-api/wikis/home