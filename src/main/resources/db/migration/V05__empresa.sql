CREATE TABLE empresa (
	id INT PRIMARY KEY AUTO_INCREMENT,
	rasao_social VARCHAR(45) NOT NULL,
	nome_fantasia VARCHAR(45) NOT NULL UNIQUE,
	cnpj INT(14),
	email VARCHAR(45) NOT NULL UNIQUE,
	senha VARCHAR(70) NOT NULL,
	telefone_id INT,
	endereco_id INT NOT NULL,
	FOREIGN KEY(telefone_id) REFERENCES telefone(id),
	FOREIGN KEY(endereco_id) REFERENCES endereco(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;