CREATE TABLE produto_encomenda (
	produto_id INT,
	encomenda_id BIGINT,
	PRIMARY KEY(produto_id, encomenda_id),
	FOREIGN KEY(produto_id) REFERENCES produto(id),
	FOREIGN KEY(encomenda_id) REFERENCES encomenda(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;