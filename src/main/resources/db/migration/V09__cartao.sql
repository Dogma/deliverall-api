CREATE TABLE cartao (
	id INT PRIMARY KEY AUTO_INCREMENT,
	bandeira VARCHAR(45),
	numero INT(16) NOT NULL,
	nome VARCHAR(45) NOT NULL,
	endereco_id INT NOT NULL,
	FOREIGN KEY(endereco_id) REFERENCES endereco(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;