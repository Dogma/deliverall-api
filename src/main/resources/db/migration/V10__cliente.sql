CREATE TABLE cliente (
	id INT PRIMARY KEY AUTO_INCREMENT,
	nome VARCHAR(45) NOT NULL,
	apelido VARCHAR(45) NOT NULL UNIQUE,
	email VARCHAR(45) NOT NULL UNIQUE,
	senha VARCHAR(70) NOT NULL,
	cartao_id INT,
	telefone_id INT,
	endereco_id INT NOT NULL,
	FOREIGN KEY(telefone_id) REFERENCES telefone(id),
	FOREIGN KEY(endereco_id) REFERENCES endereco(id),
	FOREIGN KEY(cartao_id) REFERENCES cartao(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	