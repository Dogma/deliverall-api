CREATE TABLE representante (
	id INT PRIMARY KEY AUTO_INCREMENT,
	nome VARCHAR(45) NOT NULL,
	cpf INT(11) NOT NULL,
	email VARCHAR(45) NOT NULL,
	telefone_id INT,
	endereco_id INT NOT NULL,
	FOREIGN KEY(telefone_id) REFERENCES telefone(id),
	FOREIGN KEY(endereco_id) REFERENCES endereco(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;