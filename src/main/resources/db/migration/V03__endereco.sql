CREATE TABLE endereco (
	id INT PRIMARY KEY AUTO_INCREMENT,
	cep INT(8),
	logradouro VARCHAR(45) NOT NULL,
	numero INT(6) NOT NULL,
	complemento VARCHAR(45),
	referencia VARCHAR(45),
	cidade_id INT,
	FOREIGN KEY(cidade_id) REFERENCES cidade(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;