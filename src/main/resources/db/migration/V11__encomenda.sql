CREATE TABLE encomenda (
	id BIGINT PRIMARY KEY AUTO_INCREMENT,
	empresa_id INT,
	cliente_id INT,
	valor DECIMAL(6,2) NOT NULL,
	status CHAR(1) NOT NULL,
	cidade VARCHAR(45) NOT NULL,
	cep INT(8),
	logradouro VARCHAR(45) NOT NULL,
	numero TINYINT(6) NOT NULL,
	complemento VARCHAR(45),
	FOREIGN KEY(empresa_id) REFERENCES empresa(id),
	FOREIGN KEY(cliente_id) REFERENCES cliente(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;