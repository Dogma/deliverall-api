CREATE TABLE empresa_representante (
	empresa_id INT,
	representante_id INT,
	PRIMARY KEY(empresa_id, representante_id),
	FOREIGN KEY(empresa_id) REFERENCES empresa(id),
	FOREIGN KEY(representante_id) REFERENCES representante(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;