CREATE TABLE estado (
	id INT PRIMARY KEY,
	nome varchar(60),
	uf VARCHAR(2)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO estado(id, nome, uf) VALUES
(23, 'Rio Grande do Sul', 'RS');

ALTER TABLE estado MODIFY id INT AUTO_INCREMENT;