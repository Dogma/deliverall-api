package service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deliverall.Repository.CidadeRepository;
import com.deliverall.Repository.EnderecoRepository;
import com.deliverall.model.enderecoTelefone.Cidade;
import com.deliverall.model.enderecoTelefone.Endereco;

@Service
public class EnderecoService {

	@Autowired
	private EnderecoRepository enderecoRepository;
	
	@Autowired
	private CidadeRepository cidadeRepository;
	
	public Endereco cadastrarEndereco(Endereco endereco) {
		if (endereco.getCidade() != null)
		endereco.setCidade(buscarCidade(endereco.getCidade().getNome()));
		Endereco enderecoSalvo = enderecoRepository.save(endereco);
		return enderecoSalvo;
	}
	
	public Cidade buscarCidade(String nome) {
		Cidade cidade = cidadeRepository.findByNome(nome);
		return cidade;
	}
}
