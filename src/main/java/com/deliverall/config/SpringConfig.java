package com.deliverall.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import service.EnderecoService;

@Configuration
public class SpringConfig {

	@Bean
	public EnderecoService enderecoService() {
		return new EnderecoService();
	}
}
