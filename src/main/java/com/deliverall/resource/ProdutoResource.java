package com.deliverall.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.deliverall.Repository.ProdutoRepository;
import com.deliverall.model.Produto;

@RestController
@RequestMapping("/produto")
public class ProdutoResource {
	
	private UtilResource utilResource = new UtilResource();

	@Autowired
	private ProdutoRepository produtoRepository;
	
	@CrossOrigin
	@GetMapping
	public ResponseEntity<?> listar() {
		List<Produto> produtos = produtoRepository.findAll();
		return produtos.isEmpty() ? ResponseEntity.noContent().build() : ResponseEntity.ok(produtos);
	}
	
	@CrossOrigin
	@GetMapping("/{id}")
	public ResponseEntity<?> pesquisar(@PathVariable int id) {
		Produto produto = produtoRepository.findById(id).get();
		return ResponseEntity.ok(produto);
	}
	
	@CrossOrigin
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> criar(@Valid @RequestBody Produto produto) {
		Produto produtoSalvo = produtoRepository.save(produto);
		return utilResource.criarLocation(produtoSalvo, produtoSalvo.getId());
	}
}
