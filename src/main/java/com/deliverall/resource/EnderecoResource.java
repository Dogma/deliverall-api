package com.deliverall.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.deliverall.Repository.EnderecoRepository;
import com.deliverall.model.enderecoTelefone.Endereco;

@RestController
@RequestMapping("/endereco")
public class EnderecoResource {
	
	@Autowired
	private EnderecoRepository enderecoRepository;
	
	@CrossOrigin
	@GetMapping
	public ResponseEntity<?> listar() {
		List<Endereco> enderecos = enderecoRepository.findAll();
		return enderecos.isEmpty() ? ResponseEntity.noContent().build() : ResponseEntity.ok(enderecos);
	}
	
	@CrossOrigin
	@GetMapping("/{id}")
	public ResponseEntity<?> pesquisa(@PathVariable int id) {
		Endereco endereco = enderecoRepository.findById(id).get();
		return ResponseEntity.ok(endereco);
	}

}
