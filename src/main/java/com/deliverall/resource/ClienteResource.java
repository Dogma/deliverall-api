package com.deliverall.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.deliverall.Repository.ClienteRepository;
import com.deliverall.Repository.TelefoneRepository;
import com.deliverall.model.Cliente;

import service.EnderecoService;

@RestController
@RequestMapping("/cliente")
public class ClienteResource {
	
	private UtilResource utilResource = new UtilResource();

	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private EnderecoService enderecoService;
	
	@Autowired
	private TelefoneRepository telefoneRepository;
	
	@CrossOrigin
	@GetMapping
	public ResponseEntity<?> listar() {
		List<Cliente> clientes = clienteRepository.findAll();
		return clientes.isEmpty() ? ResponseEntity.noContent().build() : ResponseEntity.ok(clientes);
	}
	
	@CrossOrigin
	@GetMapping("/{id}")
	public ResponseEntity<?> pesquisa(@PathVariable int id) {
		Cliente cliente = clienteRepository.findById(id).get();
		return ResponseEntity.ok(cliente);
	}
	
	@CrossOrigin
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> criar(@Valid @RequestBody Cliente cliente) {
		cliente.setEndereco(enderecoService.cadastrarEndereco(cliente.getEndereco()));
		if (cliente.getTelefone() != null) telefoneRepository.save(cliente.getTelefone());
		Cliente clienteSalvo = clienteRepository.save(cliente);
		return utilResource.criarLocation(clienteSalvo, clienteSalvo.getId());
	}
	
	@CrossOrigin
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable int id) {
		clienteRepository.deleteById(id);
	}

}
