package com.deliverall.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.deliverall.Repository.EmpresaRepository;
import com.deliverall.model.Empresa;

@RestController
@RequestMapping("/empresa")
public class EmpresaResource {

	@Autowired
	private EmpresaRepository empresaRepository;
	
	@CrossOrigin
	@GetMapping
	public ResponseEntity<?> listar() {
		List<Empresa> empresas = empresaRepository.findAll();
		return empresas.isEmpty() ? ResponseEntity.noContent().build() : ResponseEntity.ok(empresas);
	}
	
	
	public ResponseEntity<?> pesquisa(@PathVariable int id) {
		Empresa empresa = empresaRepository.findById(id).get();
		return ResponseEntity.ok(empresa);
	}
}
