package com.deliverall.resource;

import java.net.URI;

import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

public class UtilResource {

	public ResponseEntity<?> criarLocation(Object resource, int id) {
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}")
				.buildAndExpand(id).toUri();
		return ResponseEntity.created(uri).body(resource);
	}
}
