package com.deliverall.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.deliverall.model.enderecoTelefone.Endereco;

public interface EnderecoRepository extends JpaRepository<Endereco, Integer> {

}
