package com.deliverall.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.deliverall.model.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Integer> {

}
