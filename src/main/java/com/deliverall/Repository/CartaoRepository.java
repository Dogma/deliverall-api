package com.deliverall.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.deliverall.model.Cartao;

public interface CartaoRepository extends JpaRepository<Cartao, Integer> {

}
