package com.deliverall.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.deliverall.model.Encomenda;

public interface EncomendaRepository extends JpaRepository<Encomenda, Long> {

}
