package com.deliverall.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.deliverall.model.enderecoTelefone.Estado;

public interface EstadoRepository extends JpaRepository<Estado, Integer> {

}
