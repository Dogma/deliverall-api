package com.deliverall.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.deliverall.model.Empresa;

public interface EmpresaRepository extends JpaRepository<Empresa, Integer> {

}
