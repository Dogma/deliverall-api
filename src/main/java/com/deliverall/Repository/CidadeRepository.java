package com.deliverall.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.deliverall.model.enderecoTelefone.Cidade;

@Repository
public interface CidadeRepository extends JpaRepository<Cidade, Integer>{

	Cidade findByNome(String nome);
}
