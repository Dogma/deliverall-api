package com.deliverall.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.deliverall.model.enderecoTelefone.Telefone;

public interface TelefoneRepository extends JpaRepository<Telefone, Integer> {

}
