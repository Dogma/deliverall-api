package com.deliverall.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.deliverall.model.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Integer> {

}
