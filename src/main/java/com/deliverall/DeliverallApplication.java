package com.deliverall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeliverallApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeliverallApplication.class, args);
	}

}
